public class persegiPanjang {
    
    //deklarasi atribut
    public double panjang;
    public double lebar;
    private double diagonal;
    private double luas;
    private double keliling;

    public double getLuas(){
        luas = panjang * lebar;
        return luas;
    }
    public double getKeliling(){
        keliling = (panjang + lebar) * 2;
        return keliling;
    }
    public double getDiagonal(){
        diagonal = Math.sqrt(Math.pow(panjang, 2) + Math.pow(lebar, 2));
        return diagonal;
    }

}