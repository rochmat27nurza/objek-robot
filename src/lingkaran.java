public class lingkaran {

    //deklarasi atribut
    public static double jari_jari;
    private static double luas;
    private static double keliling;

    //behavior
    public static double getLuas(){
        double luas = Math.pow(jari_jari, 2)* Math.PI ;
        return luas;
    }

    public static double getKeliling(){
        double keliling = (2 * jari_jari) * Math.PI;
        return keliling; 
    }

}