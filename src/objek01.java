public class objek01 {
    public static void main(String[] args) {
        System.out.println("--------------------------------------------");
        System.out.println("\t\tObject Robot\t\t");
        System.out.println("--------------------------------------------");

        //Lingkaran Kepala
        System.out.println("Lingkaran Kepala");
        lingkaran LingkaranKepalla = new lingkaran();
        LingkaranKepalla.jari_jari = 3;
        System.out.printf("Luas = %.1f \n", LingkaranKepalla.getLuas());
        System.out.println();

        //Lingkaran Telapak Tangan Kiri
        System.out.println("Lingkaran Telapak Tangan Kiri");
        lingkaran LingkaranTelapakTanganKiri = new lingkaran();
        LingkaranTelapakTanganKiri.jari_jari = 2;
        System.out.printf("Luas = %.1f \n", LingkaranTelapakTanganKiri.getLuas());
        System.out.println();

        //Lingkaran Telapak Tangan Kanan
        System.out.println("Lingkaran Telapak Tangan Kanan");
        lingkaran LingkaranTelapakTanganKanan = new lingkaran();
        LingkaranTelapakTanganKanan.jari_jari = 2;
        System.out.printf("Luas = %.1f \n", LingkaranTelapakTanganKanan.getLuas());
        System.out.println();
    
        //Lingkaran Telapak Kaki Kiri
        System.out.println("Lingkaran Telapak Kaki Kiri");
        lingkaran LingkaranTelapakKakiKiri = new lingkaran();
        LingkaranTelapakKakiKiri.jari_jari = 3;
        System.out.printf("Luas = %.1f \n", LingkaranTelapakKakiKiri.getLuas());
        System.out.println();
    
        //Lingkaran Telapak Kaki Kanan
        System.out.println("Lingkaran Telapak Kaki Kanan");
        lingkaran LingkaranTelapakKakiKanan = new lingkaran();
        LingkaranTelapakKakiKanan.jari_jari = 3;
        System.out.printf("Luas = %.1f \n", LingkaranTelapakKakiKanan.getLuas());
        System.out.println();

        //Persegi Panjang Badan
        System.out.println("Persegi Panjang Badan");
        persegiPanjang PersegiPanjangBadan = new persegiPanjang();
        PersegiPanjangBadan.panjang = 20;
        PersegiPanjangBadan.lebar = 15;
        System.out.println("Luas = " + PersegiPanjangBadan.getLuas());
        System.out.println();

        //Persegi panjang tangan kiri
        System.out.println("Persegi Panjang Tangan Kiri");
        persegiPanjang PersegiPanjangTanganKiri = new persegiPanjang();
        PersegiPanjangTanganKiri.panjang = 22;
        PersegiPanjangTanganKiri.lebar = 3;
        System.out.println("Luas = " + PersegiPanjangTanganKiri.getLuas());
        System.out.println();

        //Persegi panjang tangan kanan
        System.out.println("Persegi Panjang Tangan Kanan");
        persegiPanjang PersegiPanjangTanganKanan = new persegiPanjang();
        PersegiPanjangTanganKanan.panjang = 22;
        PersegiPanjangTanganKanan.lebar = 3;
        System.out.println("Luas = " + PersegiPanjangTanganKanan.getLuas());
        System.out.println();

        //Persegi panjang kaki kiri
        System.out.println("Persegi Panjang Kaki Kiri");
        persegiPanjang PersegiPanjangKakiKiri = new persegiPanjang();
        PersegiPanjangKakiKiri.panjang = 30;
        PersegiPanjangKakiKiri.lebar = 5;
        System.out.println("Luas = " + PersegiPanjangKakiKiri.getLuas());
        System.out.println();

        //Persegi panjang kaki kanan
        System.out.println("Persegi Panjang Kaki Kanan");
        persegiPanjang PersegiPanjangKakiKanan = new persegiPanjang();
        PersegiPanjangKakiKanan.panjang = 30;
        PersegiPanjangKakiKanan.lebar = 5;
        System.out.println("Luas = " + PersegiPanjangKakiKanan.getLuas());

        //Total luas tiap bangun datar
        //lingkaran
        
        double luasTotalLingkaran = LingkaranKepalla.getLuas() +
                                    LingkaranTelapakKakiKanan.getLuas() + 
                                    LingkaranTelapakKakiKiri.getLuas() + 
                                    LingkaranTelapakTanganKanan.getLuas() + 
                                    LingkaranTelapakTanganKiri.getLuas();
        //persegi
        double luasTotalPersegiPanjang = PersegiPanjangBadan.getLuas() + 
                                         PersegiPanjangKakiKanan.getLuas() + 
                                         PersegiPanjangKakiKiri.getLuas() + 
                                         PersegiPanjangTanganKanan.getLuas() + 
                                         PersegiPanjangTanganKiri.getLuas();
        //tinggi badan robot
        double tinggiBadan = (2 * LingkaranKepalla.jari_jari ) + 
                             PersegiPanjangBadan.panjang + 
                             PersegiPanjangKakiKanan.panjang + 
                             (2 * LingkaranTelapakKakiKanan.jari_jari);


        System.out.println("--------------------------------------------");
        //total luas semua
        System.out.printf("Luas Badan Robot     : %.2f\n", (luasTotalLingkaran + luasTotalPersegiPanjang));
        System.out.printf("Tinggi Badan Robot   : %.2f\n", tinggiBadan);
        System.out.println("--------------------------------------------");

        




        
    }
}